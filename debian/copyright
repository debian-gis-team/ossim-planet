Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: OSSIM Planet
Upstream-Contact: OSSIM Developers <ossim-developer@lists.sourceforge.net>
Source: https://download.osgeo.org/ossim/source/
 The upstream tarball is repacked to exclude all subdirectories other than
 those for ossimPlanet.
Files-Excluded: csmApi/*
 csm_plugins/*
 gsoc/*
 libwms/*
 oms/*
 ossim/*
 ossimGui/*
 ossimPlanetQt/*
 ossimPredator/*
 ossim_package_support/*
 ossim_plugins/*
 ossim_qt4/*
 ossimjni/*
 planet_message/*
 pqe/*

Files: ossimPlanet/*
Copyright: 2015
License: Expat

Files: ossimPlanet/include/ossimPlanet/compiler.h
 ossimPlanet/include/ossimPlanet/netBuffer.h
 ossimPlanet/include/ossimPlanet/netChannel.h
 ossimPlanet/include/ossimPlanet/netChat.h
 ossimPlanet/include/ossimPlanet/netMessage.h
 ossimPlanet/include/ossimPlanet/netMonitor.h
 ossimPlanet/include/ossimPlanet/netSocket.h
 ossimPlanet/include/ossimPlanet/ul.h
 ossimPlanet/include/ossimPlanet/ulLocal.h
 ossimPlanet/include/ossimPlanet/ulRTTI.h
 ossimPlanet/src/ossimPlanet/netBuffer.cpp
 ossimPlanet/src/ossimPlanet/netChannel.cpp
 ossimPlanet/src/ossimPlanet/netChat.cpp
 ossimPlanet/src/ossimPlanet/netMessage.cpp
 ossimPlanet/src/ossimPlanet/netMonitor.cpp
 ossimPlanet/src/ossimPlanet/netSocket.cpp
 ossimPlanet/src/ossimPlanet/ul.cpp
 ossimPlanet/src/ossimPlanet/ulClock.cpp
 ossimPlanet/src/ossimPlanet/ulError.cpp
 ossimPlanet/src/ossimPlanet/ulLinkedList.cpp
 ossimPlanet/src/ossimPlanet/ulList.cpp
 ossimPlanet/src/ossimPlanet/ulRTTI.cpp
Copyright: 1998,2002, Steve Baker
License: LGPL-2+

Files: ossimPlanet/include/ossimPlanet/ioapi.h
 ossimPlanet/include/ossimPlanet/iowin32.h
 ossimPlanet/include/ossimPlanet/unzip.h
 ossimPlanet/include/ossimPlanet/zip.h
 ossimPlanet/src/ossimPlanet/crypt.h
 ossimPlanet/src/ossimPlanet/ioapi.c
 ossimPlanet/src/ossimPlanet/iowin32.c
 ossimPlanet/src/ossimPlanet/unzip.c
 ossimPlanet/src/ossimPlanet/zip.c
Copyright: 1998-2005, Gilles Vollant
           1990-2000, Info-ZIP.
License: zlib

Files: ossimPlanet/include/ossimPlanet/iochannel.h
 ossimPlanet/include/ossimPlanet/sg_file.h
 ossimPlanet/include/ossimPlanet/sg_socket.h
 ossimPlanet/include/ossimPlanet/sg_socket_udp.h
 ossimPlanet/src/ossimPlanet/iochannel.cpp
 ossimPlanet/src/ossimPlanet/sg_file.cpp
 ossimPlanet/src/ossimPlanet/sg_socket.cpp
 ossimPlanet/src/ossimPlanet/sg_socket_udp.cpp
Copyright: 1999, 2001, Curtis L. Olson - http://www.flightgear.org/~curt
License: GPL-2+

Files: ossimPlanet/include/ossimPlanet/ossimPlanetCompass.h
 ossimPlanet/src/ossimPlanet/ossimPlanetCompass.cpp
Copyright: 2007, Arizona State University
License: BSD-2-Clause

Files: ossimPlanet/include/ossimPlanet/ossimPlanetExport.h
Copyright: 2004, Garrett Potts
License: WMSGPL

Files: ossimPlanet/include/ossimPlanet/ossimPlanetFadeText.h
 ossimPlanet/include/ossimPlanet/ossimPlanetManipulator.h
 ossimPlanet/src/ossimPlanet/ossimPlanetFadeText.cpp
Copyright: 1998-2006, Robert Osfield
License: OSGPL

Files: debian/*
Copyright: 2016, Massimo Di Stefano <epiesasha@me.com>
License: GPL-3+

License: BSD-2-Clause
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are met:
 .
 1. Redistributions of source code must retain the above copyright notice,
    this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright notice,
    this list of conditions and the following disclaimer in the documentation
    and/or other materials provided with the distribution.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 POSSIBILITY OF SUCH DAMAGE.

License: Expat
 Permission is hereby granted, free of charge, to any person obtaining a
 copy of this software and associated documentation files (the "Software"),
 to deal in the Software without restriction, including without limitation
 the rights to use, copy, modify, merge, publish, distribute, sublicense,
 and/or sell copies of the Software, and to permit persons to whom the
 Software is furnished to do so, subject to the following conditions:
 .
 The above copyright notice and this permission notice shall be included
 in all copies or substantial portions of the Software.
 .
 THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING
 FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER
 DEALINGS IN THE SOFTWARE.

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 2 can be found in the file
 `/usr/share/common-licenses/GPL-2'.

License: GPL-3+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 On Debian systems, the full text of the GNU General Public License
 version 3 can be found in the file
 `/usr/share/common-licenses/GPL-3'.

License: LGPL-2+
 This library is free software; you can redistribute it and/or
 modify it under the terms of the GNU Library General Public
 License as published by the Free Software Foundation; either
 version 2 of the License, or (at your option) any later version.
 .
 This library is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 Library General Public License for more details.
 .
 On Debian systems, the full text of the GNU Library General Public
 License version 2 can be found in the file
 `/usr/share/common-licenses/LGPL-2'.

License: zlib
 This software is provided 'as-is', without any express or implied
 warranty.  In no event will the authors be held liable for any damages
 arising from the use of this software.
 .
 Permission is granted to anyone to use this software for any purpose,
 including commercial applications, and to alter it and redistribute it
 freely, subject to the following restrictions:
 .
 1. The origin of this software must not be misrepresented; you must not
    claim that you wrote the original software. If you use this software
    in a product, an acknowledgment in the product documentation would be
    appreciated but is not required.
 2. Altered source versions must be plainly marked as such, and must not be
    misrepresented as being the original software.
 3. This notice may not be removed or altered from any source distribution.

License: OSGPL
               OpenSceneGraph Public License, Version 0.0
               ==========================================
 .
 Copyright (C) 2002 Robert Osfield.
 .
 Everyone is permitted to copy and distribute verbatim copies
 of this licence document, but changing it is not allowed.
 .
                      OPENSCENEGRAPH PUBLIC LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 This library is free software; you can redistribute it and/or modify it
 under the terms of the OpenSceneGraph Public License (OSGPL) version 0.0
 or later.
 .
 Notes: the OSGPL is based on the LGPL, with the 4 exceptions laid
 out in the wxWindows section below.  The LGPL is contained in the
 final section of this license.
 .
 .
 -----------------------------------------------------------------------------
 .
               wxWindows Library Licence, Version 3
               ====================================
 .
 Copyright (C) 1998 Julian Smart, Robert Roebling [, ...]
 .
 Everyone is permitted to copy and distribute verbatim copies
 of this licence document, but changing it is not allowed.
 .
                      WXWINDOWS LIBRARY LICENCE
    TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
 This library is free software; you can redistribute it and/or modify it
 under the terms of the GNU Library General Public Licence as published by
 the Free Software Foundation; either version 2 of the Licence, or (at
 your option) any later version.
 .
 This library is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
 General Public Licence for more details.
 .
 You should have received a copy of the GNU Library General Public Licence
 along with this software, usually in a file named COPYING.LIB.  If not,
 write to the Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 Boston, MA 02110-1301, USA.
 .
 EXCEPTION NOTICE
 .
 1. As a special exception, the copyright holders of this library give
 permission for additional uses of the text contained in this release of
 the library as licenced under the wxWindows Library Licence, applying
 either version 3 of the Licence, or (at your option) any later version of
 the Licence as published by the copyright holders of version 3 of the
 Licence document.
 .
 2. The exception is that you may use, copy, link, modify and distribute
 under the user's own terms, binary object code versions of works based
 on the Library.
 .
 3. If you copy code from files distributed under the terms of the GNU
 General Public Licence or the GNU Library General Public Licence into a
 copy of this library, as this licence permits, the exception does not
 apply to the code that you add in this way.  To avoid misleading anyone as
 to the status of such modified files, you must delete this exception
 notice from such code and/or adjust the licensing conditions notice
 accordingly.
 .
 4. If you write modifications of your own for this library, it is your
 choice whether to permit this exception to apply to your modifications.
 If you do not wish that, you must delete the exception notice from such
 code and/or adjust the licensing conditions notice accordingly.
 .
 See also /usr/share/common-licenses/LGPL-2.1

License: WMSGPL
                 libwms Public License, Version 0.0
                 ==========================================
 .
   Copyright (C) 2004 Garrett Potts.
 .
   Everyone is permitted to copy and distribute verbatim copies
   of this licence document, but changing it is not allowed.
 .
                        LIBWMS PUBLIC LICENCE
      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
   This library is free software; you can redistribute it and/or modify it
   under the terms of the libwms Public License (WMSGPL) version 0.0
   or later.
 .
   Notes: the OSGPL is based on the LGPL, with the 4 exceptions laid in
   the wxWindows section below.  The LGPL in the final section of this
   license.
 .
 .
 -------------------------------------------------------------------------------
 .
                 wxWindows Library Licence, Version 3
                 ====================================
 .
   Copyright (C) 1998 Julian Smart, Robert Roebling [, ...]
 .
   Everyone is permitted to copy and distribute verbatim copies
   of this licence document, but changing it is not allowed.
 .
                        WXWINDOWS LIBRARY LICENCE
      TERMS AND CONDITIONS FOR COPYING, DISTRIBUTION AND MODIFICATION
 .
   This library is free software; you can redistribute it and/or modify it
   under the terms of the GNU Library General Public Licence as published by
   the Free Software Foundation; either version 2 of the Licence, or (at
   your option) any later version.
 .
   This library is distributed in the hope that it will be useful, but
   WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Library
   General Public Licence for more details.
 .
   You should have received a copy of the GNU Library General Public Licence
   along with this software, usually in a file named COPYING.LIB.  If not,
   write to the Free Software Foundation, Inc., 51 Franklin Street,
   Fifth Floor, Boston, MA 02110-1301 USA.
 .
   EXCEPTION NOTICE
 .
   1. As a special exception, the copyright holders of this library give
   permission for additional uses of the text contained in this release of
   the library as licenced under the wxWindows Library Licence, applying
   either version 3 of the Licence, or (at your option) any later version of
   the Licence as published by the copyright holders of version 3 of the
   Licence document.
 .
   2. The exception is that you may use, copy, link, modify and distribute
   under the user's own terms, binary object code versions of works based
   on the Library.
 .
   3. If you copy code from files distributed under the terms of the GNU
   General Public Licence or the GNU Library General Public Licence into a
   copy of this library, as this licence permits, the exception does not
   apply to the code that you add in this way.  To avoid misleading anyone as
   to the status of such modified files, you must delete this exception
   notice from such code and/or adjust the licensing conditions notice
   accordingly.
 .
   4. If you write modifications of your own for this library, it is your
   choice whether to permit this exception to apply to your modifications.
   If you do not wish that, you must delete the exception notice from such
   code and/or adjust the licensing conditions notice accordingly.
 .
 .
 ------------------------------------------------------------------------------

